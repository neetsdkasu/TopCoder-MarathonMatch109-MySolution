Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic

Imports Solution = System.Tuple(Of Long, Integer())
Imports Tp = System.Tuple(Of Integer, Integer)
Imports Tp3 = System.Tuple(Of Integer, Integer, Integer)

Public Class RoadNetwork

    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)

    Dim NM, N As Integer
    Dim Edges() As Edge
    Dim Routes() As Edge
    Dim EdgeCount As Integer
    Dim RouteCount As Integer
    Dim RefEdges(,) As Edge
    Dim RefRoutes(,) As Edge
    Dim Graph() As List(Of Integer)

    Public Function findSolution(NM As Integer, N As Integer, E As Integer, edges() As String, R As Integer, routes() As String) As Integer()
        startTime = Environment.TickCount
        limitTime = startTime + 9800
        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))
        Console.Error.WriteLine("NM: {0}, N: {1}, E: {2}, R: {3}", NM, N, E, R)

        Me.NM = NM
        Me.N = N
        Me.EdgeCount = E
        Me.RouteCount = R

        ReDim Me.Graph(N - 1)
        For i As Integer = 0 To N - 1
            Me.Graph(i) = New List(Of Integer)()
        Next i

        ReDim Me.Edges(E - 1)
        Redim Me.RefEdges(N - 1, N - 1)
        For i As Integer = 0 To E - 1
            Dim eg As New Edge(i, edges(i))
            Me.Edges(i) = eg
            Me.RefEdges(eg.A, eg.B) = eg
            Me.RefEdges(eg.B, eg.A) = eg
            Me.Graph(eg.A).Add(eg.B)
            Me.Graph(eg.B).Add(eg.A)
        Next i

        ReDim Me.Routes(R - 1)
        Redim Me.RefRoutes(N - 1, N - 1)
        For i As Integer = 0 To R - 1
            Dim rt As New Edge(i, routes(i))
            Me.Routes(i) = rt
            Me.RefRoutes(rt.A, rt.B) = rt
            Me.RefRoutes(rt.B, rt.A) = rt
        Next i

        Array.Sort(Me.Routes, Function(e1 As Edge, e2 As Edge) As Integer
            Return e2.Dist.CompareTo(e1.Dist)
        End Function)

        Dim interval, startT, endT As Integer

        startT = Environment.TickCount
        Dim ret As Solution = Solve()
        endT = Environment.TickCount
        interval = (endT - startT) * 107 \ 100
        Console.Error.WriteLine("sc1: {0}, time1: {1}", ret.Item1, endT - startT)

        If endT + interval < limitTime Then
            Array.Sort(Me.Routes, Function(e1 As Edge, e2 As Edge) As Integer
                Return e1.Dist.CompareTo(e2.Dist)
            End Function)
            startT = Environment.TickCount
            Dim tmp As Solution = Solve()
            endT = Environment.TickCount
            interval = Math.Max(interval, (endT - startT) * 107 \ 100)
            If tmp.Item1 > ret.Item1 Then
                ret = tmp
                Console.Error.WriteLine("sc2: {0}, time2: {1}", tmp.Item1, endT - startT)
            End If
        End If

        For cycle = 3 To 500000
            startT = Environment.TickCount
            If startT + interval >= limitTime Then
                Console.Error.WriteLine("cycle: {0}, interval: {1}", cycle, interval)
                Exit For
            End If
            Shuffle(Me.Routes)
            Dim tmp As Solution = Solve()
            endT = Environment.TickCount
            interval = Math.Max(interval, (endT - startT) * 107 \ 100)
            If tmp.Item1 > ret.Item1 Then
                ret = tmp
                Console.Error.WriteLine("sc{2}: {0}, time{2}: {1}", tmp.Item1, endT - startT, cycle)
            End If
        Next cycle

        findSolution = ret.Item2
    End Function

    Private Function Solve() As Solution
        Dim NM As Integer = Me.NM

        Dim ret As New List(Of Integer)()
        Dim routeScore As Integer = 0
        Dim edgeScore As Integer = 0

        Dim usedRoad(EdgeCount - 1) As Boolean

        For r As Integer = 0 To UBound(Routes)
            Dim rt As Edge = Routes(r)
            Dim targets As New SortedSet(Of Tp3)()
            Dim dist(N - 1) As Integer
            Dim points(N - 1) As Integer
            Dim parent(N - 1) As Integer
            Fill(dist, 100000)
            dist(rt.A) = 0
            parent(rt.A) = rt.A
            targets.Add(New Tp3(0, 0, rt.A))
            Do While targets.Count > 0
                Dim m As Tp3 = targets.Min
                targets.Remove(m)
                If m.Item1 > dist(m.Item3) Then Continue Do
                If m.Item3 = rt.B Then Exit Do
                For Each id As Integer In Graph(m.Item3)
                    Dim d As Integer = m.Item1
                    Dim p As Integer = m.Item2
                    Dim eg As Edge = RefEdges(id, m.Item3)
                    If Not usedRoad(eg.Id) Then
                        d += eg.Dist
                        p -= eg.Points
                    End If
                    Select Case d.CompareTo(dist(id))
                    Case 0
                        If p >= points(id) Then Continue For
                    Case 1
                        Continue For
                    End Select
                    dist(id) = d
                    points(id) = p
                    parent(id) = m.Item3
                    targets.Add(New Tp3(d, p, id))
                Next id
            Loop
            If dist(rt.B) > NM Then Continue For
            Dim cur As Integer = rt.B
            Do While parent(cur) <> cur
                Dim p As Integer = parent(cur)
                Dim id As Integer = RefEdges(p, cur).Id
                If Not usedRoad(id) Then
                    ret.Add(id)
                    usedRoad(id) = True
                    edgeScore += RefEdges(p, cur).Points
                End If
                cur = p
            Loop
            NM -= dist(rt.B)
            routeScore += rt.Points
        Next r

        edgeScore += DPFillRemainRoads(NM, usedRoad, ret)

        Solve = New Solution(CLng(edgeScore) * CLng(routeScore), ret.ToArray())
    End Function

    Private Function DPFillRemainRoads(NM As Integer, usedRoad() As Boolean, ret As List(Of Integer)) As Integer
        Dim dpCosts(1000) As Integer
        Dim dpEdges(1000) As Node(Of Integer)
        Dim edgeScore As Integer = 0
        dpEdges(0) = New Node(Of Integer)(0, Nothing)
        For e As Integer = 0 To UBound(Edges)
            Dim eg As Edge = Edges(e)
            If usedRoad(eg.Id) Then Continue For
            If eg.Dist > NM Then Continue For
            For i As Integer = UBound(dpCosts) - eg.Points To 0 Step -1
                If dpEdges(i) Is Nothing Then Continue For
                Dim p As Integer = i + eg.Points
                Dim c As Integer = dpCosts(i) + eg.Dist
                If c > NM Then Continue For
                If c < dpCosts(p) OrElse dpCosts(p) = 0 Then
                    dpCosts(p) = c
                    dpEdges(p) = New Node(Of Integer)(eg.Id, dpEdges(i))
                End If
            Next i
        Next e
        For i As Integer = UBound(dpEdges) To 1 Step -1
            If dpEdges(i) Is Nothing Then Continue For
            Dim nd As Node(Of Integer) = dpEdges(i)
            Do While nd.Parent IsNot Nothing
                ret.Add(nd.Value)
                edgeScore += Edges(nd.Value).Points
                nd = nd.Parent
            Loop
            Exit For
        Next i
        DPFillRemainRoads = edgeScore
    End Function

    Private Sub Shuffle(Of T)(arr() As T)
        For i As Integer = UBound(arr) To 1 Step -1
            Dim j As Integer = rand.Next(i + 1)
            Dim tmp As T = arr(j)
            arr(j) = arr(i)
            arr(i) = tmp
        Next i
    End Sub

    Private Sub Fill(Of T)(arr() As T, value As T)
        For i As Integer = 0 To UBound(arr)
            arr(i) = value
        Next i
    End Sub
End Class

Class Edge
    Public ReadOnly Id As Integer
    Public ReadOnly A As Integer
    Public ReadOnly B As Integer
    Public ReadOnly Dist As Integer
    Public ReadOnly Points As Integer
    Public Sub New(Id As Integer, s As String)
        Me.Id = Id
        Dim ss() As String = s.Split(" "c)
        A = CInt(ss(0))
        B = CInt(ss(1))
        If B < A Then
            Dim t As Integer = A
            A = B
            B = t
        End If
        If ss.Length = 4 Then
            Dist = CInt(ss(2))
            Points = CInt(ss(3))
        Else
            Dist = CInt(ss(2))
            Points = Dist
        End If
    End Sub
    Public Sub New(Id As Integer, A As Integer, B As Integer, Dist As Integer, Points As Integer)
        Me.Id = Id
        If A < B Then
            Me.A = A
            Me.B = B
        Else
            Me.A = B
            Me.B = A
        End If
        Me.Dist = Dist
        Me.Points = Points
    End Sub
End Class

Class Node(Of T)
    Public ReadOnly Value As T
    Public ReadOnly Parent As Node(Of T)
    Public Sub New(v As T, p As Node(Of T))
        Me.Value = v
        Me.Parent = p
    End Sub
End Class