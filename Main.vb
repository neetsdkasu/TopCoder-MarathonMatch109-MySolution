Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports Console = System.Console
Imports Stopwatch = System.Diagnostics.Stopwatch

Public Module Main

    Dim sw As New Stopwatch()

    Sub PrintTime()
        Console.Error.WriteLine("time: {0} ms", sw.ElapsedMilliseconds)
    End Sub

    Sub Main(args() As String)
        Try
            sw.Start()

            Dim _roadNetwork As New RoadNetwork()

            sw.Stop()

            ' do edit codes if you need

            CallFindSolution(_roadNetwork)

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

    Function CallFindSolution(_roadNetwork As RoadNetwork) As Integer
        Dim NM As Integer = CInt(Console.ReadLine())
        Dim N As Integer = CInt(Console.ReadLine())
        ' Dim E As Integer = CInt(Console.ReadLine())
        Dim _edgesSize As Integer = CInt(Console.ReadLine()) - 1
        Dim E As Integer = _edgesSize + 1
        Dim edges(_edgesSize) As String
        For _idx As Integer = 0 To _edgesSize
            edges(_idx) = Console.ReadLine()
        Next _idx
        ' Dim R As Integer = CInt(Console.ReadLine())
        Dim _routesSize As Integer = CInt(Console.ReadLine()) - 1
        Dim R As Integer = _routesSize + 1
        Dim routes(_routesSize) As String
        For _idx As Integer = 0 To _routesSize
            routes(_idx) = Console.ReadLine()
        Next _idx

        sw.Start()

        Dim _result As Integer() = _roadNetwork.findSolution(NM, N, E, edges, R, routes)

        sw.Stop()
        PrintTime()

        Console.WriteLine(_result.Length)
        For Each _it As Integer In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function
End Module